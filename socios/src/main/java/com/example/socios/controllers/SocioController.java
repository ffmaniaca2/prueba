package com.example.socios.controllers;

import com.example.socios.models.Socio;
import com.example.socios.services.SocioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("socios")
public class SocioController {
    private final SocioService socioService;

    @Autowired
    public SocioController(SocioService socioService) {
        this.socioService = socioService;
    }

    @GetMapping()
    public ResponseEntity<List<Socio>> socio() {
        System.out.println("Me piden la lista de socio");
        return ResponseEntity.ok(socioService.findAll());
    }

    @PostMapping()
    public ResponseEntity<Socio> saveVehiculo(@RequestBody Socio socio)
    {
        return ResponseEntity.ok(socioService.saveSocio(socio));
    }
}
