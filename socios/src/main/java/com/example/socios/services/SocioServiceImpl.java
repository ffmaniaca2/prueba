package com.example.socios.services;

import com.example.socios.models.Socio;
import com.example.socios.repository.SocioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("vehiculoService")
@Transactional
public class SocioServiceImpl implements SocioService {

        private SocioRepository socioRepository;

        @Autowired
        public SocioServiceImpl(SocioRepository socioRepository)
        {
            this.socioRepository = socioRepository;
        }

        @Override
        public List<Socio> findAll() {
            return socioRepository.findAll();
        }

        @Override
        public Socio findOne(String id) {
            return socioRepository.findOne(id);
        }

        @Override
        public Socio saveSocio(Socio soc) {
            return socioRepository.saveSocio(soc);
        }

        @Override
        public void updateSocio(Socio soc) {
            socioRepository.updateSocio(soc);
        }

        @Override
        public void deleteSocio(String id) {
            socioRepository.deleteSocio(id);
        }

}
