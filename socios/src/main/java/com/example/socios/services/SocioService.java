package com.example.socios.services;

import com.example.socios.models.Socio;

import java.util.List;

public interface SocioService {

    List<Socio> findAll();
    public Socio findOne(String id);
    public Socio saveSocio(Socio soc);
    public void updateSocio(Socio soc);
    public void deleteSocio(String id);
}
