package com.example.socios.repository;

import com.example.socios.models.Socio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public class SocioRepositoryImpl implements SocioRepository {
    private final MongoOperations mongoOperations;

    @Autowired
    public SocioRepositoryImpl(MongoOperations mongoOperations) {
        this.mongoOperations = mongoOperations;
    }

    @Override
    public List<Socio> findAll() {
        List<Socio> Socios = this.mongoOperations.find(new Query(), Socio.class);
        return Socios;
    }

    @Override
    public Socio findOne(String id) {
        Socio encontrado = this.mongoOperations.findOne(new Query(Criteria.where("idSocio").is(id)),Socio.class);
        return encontrado;
    }

    @Override
   // public Socio saveSocio(Socio soc) {
    public Socio saveSocio(Socio soc) {
        this.mongoOperations.save(soc);
     //   return findOne(soc.getIdSocio());
        return null;
    }

    @Override
    public void updateSocio(Socio soc) {
        this.mongoOperations.save(soc);
    }

    @Override
    public void deleteSocio(String id) {
        this.mongoOperations.findAndRemove(new Query(Criteria.where("idSocio").is(id)), Socio.class);
    }

    }



