package com.example.socios.repository;

import com.example.socios.models.Socio;

import java.util.List;

public interface SocioRepository {
    List<Socio> findAll();
    public Socio findOne(String id);
    public Socio saveSocio(Socio soc);
    public void updateSocio(Socio soc);
    public void deleteSocio(String id);

}
